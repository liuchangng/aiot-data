package com.ai.detection.service;

public interface DjlService {

    public String fireSmoke(String file);

    public String reflectiveVest(String file);

    public String vehicle(String file);

    public String cameraFacemask(String file);
}


