package net.srt.dto;

import lombok.Data;

/**
 * @ClassName sqlConsole
 * @Author zrx
 * @Date 2022/10/24 9:50
 */
@Data
public class SqlConsole {
	private String sql;
	private Long sqlKey;
}
